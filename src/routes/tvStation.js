import * as tvStationService from '../services/tvStationService';
import { findAll } from '../services/roleService';

const tvStationRoutes = () => {
    return {
        saveTvStation: {
            path: '/api/station',
            methods: ['POST'],
            callback: tvStationService.save,
        },
        updatedTvStation: {
            path: '/api/station/:id',
            methods: ['PUT'],
            callback: tvStationService.update,
        },
        findOneTvStation: {
            path: '/api/station/:id',
            methods: ['GET'],
            callback: tvStationService.findOne,
        },
        findAllTvStations: {
            path: '/api/station',
            methods: ['GET'],
            callback: tvStationService.findAll,
        },
        removeTvStation: {
            path: '/api/station/:id',
            methods: ['DELETE'],
            callback: tvStationService.remove,
        },
    };
};

module.exports = tvStationRoutes;