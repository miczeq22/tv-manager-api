import * as categoryService from '../services/categoryService';

const categoryRoutes = () => {
    return {
        saveCategory: {
            path: '/api/category',
            methods: ['POST'],
            callback: categoryService.save,
        },
        updateCategory: {
            path: '/api/category/:id',
            methods: ['PUT'],
            callback: categoryService.update,
        },
        findOneCategory: {
            path: '/api/category/:id',
            methods: ['GET'],
            callback: categoryService.findOne,
        },
        findAllCategories: {
            path: '/api/category',
            methods: ['GET'],
            callback: categoryService.findAll,
        },
        removeCategory: {
            path: '/api/category/:id',
            methods: ['DELETE'],
            callback: categoryService.remove,
        },
    };
};

module.exports = categoryRoutes;