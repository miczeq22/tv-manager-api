import expressApp, { container } from '../../index';
import chaiHttp from 'chai-http';
import chai, { expect } from 'chai';
import bcrypt from 'bcrypt';

chai.use(chaiHttp);

describe ('HTTP routes', () => {
    let server;
    let adminToken = '';

    before((done) => {
        expressApp.on('appStarted', () => {
            server = expressApp.listen(done);
        });
    });

    before(async () => {
        const roleRepository = container.take('roleRepository');
        const userRepository = container.take('userRepository');

        const passwordHash = await bcrypt.hash('admin123', 10);

        await roleRepository.save({ name: 'Admin' });
        await userRepository.save({ username: 'admin', passwordHash });

        const user = await userRepository.findOne({ where: { id: 1 }, relations: ['roles'] });
        const adminRole = await roleRepository.findOne({ where: { id: 1 } });
        
        user.roles.push(adminRole);

        await userRepository.save(user);

        const response = await chai.request(server).post('/login').send({
            username: 'admin',
            password: 'admin123',
        });

        adminToken = response.body.token;
    });

    describe ('Role routes', () => {
        describe ('[POST] /api/role', () => {
            it ('should respond with 400 status if request body is invalid', async () => {
                const response = await chai.request(server).post('/api/role').send({
                    bad: 'property',
                }).set({ Authorization: adminToken });

                expect(response.status).to.be.equal(400);
                expect(Object.keys(response.body.error)).to.be.deep.equal([
                    'message',
                ]);
            });

            it ('should respond with 201 status and proper body', async () => {
                const response = await chai.request(server).post('/api/role').send({
                    name: 'User',
                }).set({ Authorization: adminToken });

                expect(response.status).to.be.equal(201);
                expect(Object.keys(response.body.savedRole)).to.be.deep.equal([
                    'name',
                    'id',
                ]);
            });

            it ('should respond with 400 status if role already exists', async () => {
                const response = await chai.request(server).post('/api/role').send({
                    name: 'User',
                }).set({ Authorization: adminToken });

                expect(response.status).to.be.equal(400);
                expect(Object.keys(response.body.error)).to.be.deep.equal([
                    'message'
                ]);
            });
        });

        describe ('[PUT] /api/role/:id', () => {
            it ('should respond with 400 status if request body is invalid', async () => {
                const response = await chai.request(server).put('/api/role/2').send({
                    bad: 'property',
                }).set({ Authorization: adminToken });

                expect(response.status).to.be.equal(400);
                expect(Object.keys(response.body.error)).to.be.deep.equal([
                    'message',
                ]);
            });

            it ('should respond with 404 status if role does not exists', async () => {
                const response = await chai.request(server).put('/api/role/101').send({
                    name: 'user',
                }).set({ Authorization: adminToken });

                expect(response.status).to.be.equal(404);
                expect(Object.keys(response.body.error)).to.be.deep.equal([
                    'message'
                ]);
            });

            it ('should respond with 201 status and proper body', async () => {
                const response = await chai.request(server).put('/api/role/2').send({
                    name: 'user',
                }).set({ Authorization: adminToken });

                expect(response.status).to.be.equal(201);
                expect(Object.keys(response.body.updatedRole)).to.be.deep.equal([
                    'id',
                    'name',                    
                    'addedOn',
                ]);
            });
        });

        describe ('[GET] /api/role/:id', () => {
            it ('should respond with 404 status if role does not exists', async () => {
                const response = await chai.request(server).get('/api/role/101').set({ Authorization: adminToken });

                expect(response.status).to.be.equal(404);
                expect(Object.keys(response.body.error)).to.be.deep.equal([
                    'message'
                ]);
            });

            it ('should respond with 200 status and proper body', async () => {
                const response = await chai.request(server).get('/api/role/2').set({ Authorization: adminToken });

                expect(response.status).to.be.equal(200);
                expect(Object.keys(response.body.role)).to.be.deep.equal([
                    'id',
                    'name',                    
                    'addedOn',
                ]);
            });
        });

        describe ('[GET] /api/role/byName/:id', () => {
            it ('should respond with 404 status if role does not exists', async () => {
                const response = await chai.request(server).get('/api/role/byName/NotExists').set({ Authorization: adminToken });

                expect(response.status).to.be.equal(404);
                expect(Object.keys(response.body.error)).to.be.deep.equal([
                    'message'
                ]);
            });

            it ('should respond with 200 status and proper body', async () => {
                const response = await chai.request(server).get('/api/role/byName/user').set({ Authorization: adminToken });

                expect(response.status).to.be.equal(200);
                expect(Object.keys(response.body.role)).to.be.deep.equal([
                    'id',
                    'name',                    
                    'addedOn',
                ]);
            });
        });

        it ('[GET] /api/role should respond with 200 status and proper body', async () => {
            const response = await chai.request(server).get('/api/role').set({ Authorization: adminToken });

            expect(response.status).to.be.equal(200);
            expect(response.body.roles).to.be.instanceOf(Array);
        });

        describe ('[DELETE] /api/role/:id', () => {
            it ('should respond with 404 status if role does not exists', async () => {
                const response = await chai.request(server).delete('/api/role/101').set({ Authorization: adminToken });

                expect(response.status).to.be.equal(404);
                expect(Object.keys(response.body.error)).to.be.deep.equal([
                    'message'
                ]);
            });

            it ('should respond with 201 status and proper body', async () => {
                const response = await chai.request(server).delete('/api/role/2').set({ Authorization: adminToken });

                expect(response.status).to.be.equal(201);
                expect(Object.keys(response.body)).to.be.deep.equal([
                    'removedRoleId'
                ]);
            });
        });
    });

    describe ('User routes', () => {
        describe ('[POST] /api/user', () => {
            it ('should respond with 400 status if request body is invalid', async () => {
                const response = await chai.request(server).post('/api/user').send({
                    bad: 'property',
                }).set({ Authorization: adminToken });

                expect(response.status).to.be.equal(400);
                expect(Object.keys(response.body.error)).to.be.deep.equal([
                    'message',
                ]);
            });

            it ('should respond with 201 status and proper body', async () => {
                const response = await chai.request(server).post('/api/user').send({
                    username: 'user',
                    password: 'user1234'
                }).set({ Authorization: adminToken });

                expect(response.status).to.be.equal(201);
                expect(Object.keys(response.body.savedUser)).to.be.deep.equal([
                    'username',
                    'passwordHash',
                    'roles',
                    'id'
                ]);
            });

            it ('should respond with 400 status if user already exists', async () => {
                const response = await chai.request(server).post('/api/role').send({
                    username: 'user',
                    password: 'user1234'
                }).set({ Authorization: adminToken });

                expect(response.status).to.be.equal(400);
                expect(Object.keys(response.body.error)).to.be.deep.equal([
                    'message'
                ]);
            });
        });

        describe ('[PUT] /api/user/:id', () => {
            it ('should respond with 400 status if request body is invalid', async () => {
                const response = await chai.request(server).put('/api/user/2').send({
                    bad: 'property',
                }).set({ Authorization: adminToken });

                expect(response.status).to.be.equal(400);
                expect(Object.keys(response.body.error)).to.be.deep.equal([
                    'message',
                ]);
            });

            it ('should respond with 404 status if user does not exists', async () => {
                const response = await chai.request(server).put('/api/user/101').send({
                    oldPassword: 'user1234',
                    newPassword: 'user12345',
                }).set({ Authorization: adminToken });

                expect(response.status).to.be.equal(404);
                expect(Object.keys(response.body.error)).to.be.deep.equal([
                    'message'
                ]);
            });

            it ('should respond with 201 status and proper body', async () => {
                const response = await chai.request(server).put('/api/user/2').send({
                    oldPassword: 'user1234',
                    newPassword: 'user12345',
                }).set({ Authorization: adminToken });

                expect(response.status).to.be.equal(201);
                expect(Object.keys(response.body.updatedUser)).to.be.deep.equal([
                    'id',
                    'username',
                    'passwordHash',                    
                    'addedOn',
                ]);
            });
        });

        describe ('[GET] /api/user/:id', () => {
            it ('should respond with 404 status if user does not exists', async () => {
                const response = await chai.request(server).get('/api/user/101').set({ Authorization: adminToken });

                expect(response.status).to.be.equal(404);
                expect(Object.keys(response.body.error)).to.be.deep.equal([
                    'message'
                ]);
            });

            it ('should respond with 200 status and proper body', async () => {
                const response = await chai.request(server).get('/api/user/2').set({ Authorization: adminToken });

                expect(response.status).to.be.equal(200);
                expect(Object.keys(response.body.user)).to.be.deep.equal([
                    'id',
                    'username',                    
                    'passwordHash',
                    'addedOn',
                ]);
            });
        });

        it ('[GET] /api/user should respond with 200 status and proper body', async () => {
            const response = await chai.request(server).get('/api/user').set({ Authorization: adminToken });

            expect(response.status).to.be.equal(200);
            expect(response.body.users).to.be.instanceOf(Array);
        });

        describe ('[DELETE] /api/user/:id', () => {
            it ('should respond with 404 status if user does not exists', async () => {
                const response = await chai.request(server).delete('/api/user/101').set({ Authorization: adminToken });

                expect(response.status).to.be.equal(404);
                expect(Object.keys(response.body.error)).to.be.deep.equal([
                    'message'
                ]);
            });

            it ('should respond with 201 status and proper body', async () => {
                const response = await chai.request(server).delete('/api/user/2').set({ Authorization: adminToken });

                expect(response.status).to.be.equal(201);
                expect(Object.keys(response.body)).to.be.deep.equal([
                    'removedUserId'
                ]);
            });
        });
    });

    describe ('TvStation routes', () => {
        describe ('[POST] /api/station', () => {
            it ('should respond with 400 status if request body is invalid', async () => {
                const response = await chai.request(server).post('/api/station').send({
                    bad: 'property',
                }).set({ Authorization: adminToken });

                expect(response.status).to.be.equal(400);
                expect(Object.keys(response.body.error)).to.be.deep.equal([
                    'message',
                ]);
            });

            it ('should respond with 201 status and proper body', async () => {
                const response = await chai.request(server).post('/api/station').send({
                    name: 'Polsat',
                }).set({ Authorization: adminToken });
                expect(response.status).to.be.equal(201);
                expect(Object.keys(response.body.savedTvStation)).to.be.deep.equal([
                    'name',
                    'id',
                ]);
            });

            it ('should respond with 400 status if tv station already exists', async () => {
                const response = await chai.request(server).post('/api/station').send({
                    name: 'Polsat',
                }).set({ Authorization: adminToken });

                expect(response.status).to.be.equal(400);
                expect(Object.keys(response.body.error)).to.be.deep.equal([
                    'message'
                ]);
            });
        });

        describe ('[PUT] /api/station/:id', () => {
            it ('should respond with 400 status if request body is invalid', async () => {
                const response = await chai.request(server).put('/api/station/2').send({
                    bad: 'property',
                }).set({ Authorization: adminToken });

                expect(response.status).to.be.equal(400);
                expect(Object.keys(response.body.error)).to.be.deep.equal([
                    'message',
                ]);
            });

            it ('should respond with 404 status if station does not exists', async () => {
                const response = await chai.request(server).put('/api/station/101').send({
                    name: 'polsat',
                }).set({ Authorization: adminToken });

                expect(response.status).to.be.equal(404);
                expect(Object.keys(response.body.error)).to.be.deep.equal([
                    'message'
                ]);
            });

            it ('should respond with 201 status and proper body', async () => {
                const response = await chai.request(server).put('/api/station/1').send({
                    name: 'polsat',
                }).set({ Authorization: adminToken });

                expect(response.status).to.be.equal(201);
                expect(Object.keys(response.body.updatedTvStation)).to.be.deep.equal([
                    'id',
                    'name',                    
                    'addedOn',
                ]);
            });
        });

        describe ('[GET] /api/station/:id', () => {
            it ('should respond with 404 status if station does not exists', async () => {
                const response = await chai.request(server).get('/api/station/101').set({ Authorization: adminToken });

                expect(response.status).to.be.equal(404);
                expect(Object.keys(response.body.error)).to.be.deep.equal([
                    'message'
                ]);
            });

            it ('should respond with 200 status and proper body', async () => {
                const response = await chai.request(server).get('/api/station/1').set({ Authorization: adminToken });

                expect(response.status).to.be.equal(200);
                expect(Object.keys(response.body.tvStation)).to.be.deep.equal([
                    'id',
                    'name',                    
                    'addedOn',
                ]);
            });
        });

        it ('[GET] /api/station should respond with 200 status and proper body', async () => {
            const response = await chai.request(server).get('/api/station').set({ Authorization: adminToken });

            expect(response.status).to.be.equal(200);
            expect(response.body.tvStations).to.be.instanceOf(Array);
        });

        describe ('[DELETE] /api/station/:id', () => {
            it ('should respond with 404 status if station does not exists', async () => {
                const response = await chai.request(server).delete('/api/station/101').set({ Authorization: adminToken });

                expect(response.status).to.be.equal(404);
                expect(Object.keys(response.body.error)).to.be.deep.equal([
                    'message'
                ]);
            });

            it ('should respond with 201 status and proper body', async () => {
                const response = await chai.request(server).delete('/api/station/1').set({ Authorization: adminToken });

                expect(response.status).to.be.equal(201);
                expect(Object.keys(response.body)).to.be.deep.equal([
                    'removedTvStationId'
                ]);
            });
        });
    });

    describe ('Category routes', () => {
        describe ('[POST] /api/category', () => {
            it ('should respond with 400 status if request body is invalid', async () => {
                const response = await chai.request(server).post('/api/category').send({
                    bad: 'property',
                }).set({ Authorization: adminToken });

                expect(response.status).to.be.equal(400);
                expect(Object.keys(response.body.error)).to.be.deep.equal([
                    'message',
                ]);
            });

            it ('should respond with 201 status and proper body', async () => {
                const response = await chai.request(server).post('/api/category').send({
                    name: 'Komedia',
                }).set({ Authorization: adminToken });

                expect(response.status).to.be.equal(201);
                expect(Object.keys(response.body.savedCategory)).to.be.deep.equal([
                    'name',
                    'id',
                ]);
            });

            it ('should respond with 400 status if role already exists', async () => {
                const response = await chai.request(server).post('/api/category').send({
                    name: 'Komedia',
                }).set({ Authorization: adminToken });

                expect(response.status).to.be.equal(400);
                expect(Object.keys(response.body.error)).to.be.deep.equal([
                    'message'
                ]);
            });
        });

        describe ('[PUT] /api/category/:id', () => {
            it ('should respond with 400 status if request body is invalid', async () => {
                const response = await chai.request(server).put('/api/category/1').send({
                    bad: 'property',
                }).set({ Authorization: adminToken });

                expect(response.status).to.be.equal(400);
                expect(Object.keys(response.body.error)).to.be.deep.equal([
                    'message',
                ]);
            });

            it ('should respond with 404 status if category does not exists', async () => {
                const response = await chai.request(server).put('/api/category/101').send({
                    name: 'komedia',
                }).set({ Authorization: adminToken });

                expect(response.status).to.be.equal(404);
                expect(Object.keys(response.body.error)).to.be.deep.equal([
                    'message'
                ]);
            });

            it ('should respond with 201 status and proper body', async () => {
                const response = await chai.request(server).put('/api/category/1').send({
                    name: 'komedia',
                }).set({ Authorization: adminToken });

                expect(response.status).to.be.equal(201);
                expect(Object.keys(response.body.updatedCategory)).to.be.deep.equal([
                    'id',
                    'name',                    
                    'addedOn',
                ]);
            });
        });

        describe ('[GET] /api/category/:id', () => {
            it ('should respond with 404 status if role does not exists', async () => {
                const response = await chai.request(server).get('/api/category/101').set({ Authorization: adminToken });

                expect(response.status).to.be.equal(404);
                expect(Object.keys(response.body.error)).to.be.deep.equal([
                    'message'
                ]);
            });

            it ('should respond with 200 status and proper body', async () => {
                const response = await chai.request(server).get('/api/category/1').set({ Authorization: adminToken });

                expect(response.status).to.be.equal(200);
                expect(Object.keys(response.body.category)).to.be.deep.equal([
                    'id',
                    'name',                    
                    'addedOn',
                ]);
            });
        });

        it ('[GET] /api/category should respond with 200 status and proper body', async () => {
            const response = await chai.request(server).get('/api/category').set({ Authorization: adminToken });

            expect(response.status).to.be.equal(200);
            expect(response.body.categories).to.be.instanceOf(Array);
        });

        describe ('[DELETE] /api/category/:id', () => {
            it ('should respond with 404 status if role does not exists', async () => {
                const response = await chai.request(server).delete('/api/category/101').set({ Authorization: adminToken });

                expect(response.status).to.be.equal(404);
                expect(Object.keys(response.body.error)).to.be.deep.equal([
                    'message'
                ]);
            });

            it ('should respond with 201 status and proper body', async () => {
                const response = await chai.request(server).delete('/api/category/1').set({ Authorization: adminToken });

                expect(response.status).to.be.equal(201);
                expect(Object.keys(response.body)).to.be.deep.equal([
                    'removedCategoryId'
                ]);
            });
        });
    });

    describe ('TvProgram routes', () => {
        describe ('[POST] /api/program', () => {
            const program = {
                    name: 'Fake Tv Program',
                    description: 'Fake Description...',
                    rating: 3,
                    imageUrl: 'Fake img url...',
                    ageRating: 12,
            };
            it ('should respond with 400 status if request body is invalid', async () => {
                const response = await chai.request(server).post('/api/program').send({
                    bad: 'property',
                }).set({ Authorization: adminToken });

                expect(response.status).to.be.equal(400);
                expect(Object.keys(response.body.error)).to.be.deep.equal([
                    'message',
                ]);
            });

            it ('should respond with 201 status and proper body', async () => {
                const response = await chai.request(server).post('/api/program').send({
                    ...program
                }).set({ Authorization: adminToken });

                expect(response.status).to.be.equal(201);
                expect(Object.keys(response.body.savedTvProgram)).to.be.deep.equal([
                    'name',
                    'description',
                    'rating',
                    'imageUrl',
                    'ageRating',
                    'id',
                ]);
            });

            it ('should respond with 400 status if program already exists', async () => {
                const response = await chai.request(server).post('/api/program').send({
                    ...program
                }).set({ Authorization: adminToken });

                expect(response.status).to.be.equal(400);
                expect(Object.keys(response.body.error)).to.be.deep.equal([
                    'message'
                ]);
            });
        });

        describe ('[PUT] /api/category/:id', () => {
            it ('should respond with 404 status if program does not exists', async () => {
                const response = await chai.request(server).put('/api/program/101').send({
                    name: 'Fake name...',
                }).set({ Authorization: adminToken });

                expect(response.status).to.be.equal(404);
                expect(Object.keys(response.body.error)).to.be.deep.equal([
                    'message'
                ]);
            });

            it ('should respond with 201 status and proper body', async () => {
                const response = await chai.request(server).put('/api/program/1').send({
                    name: 'Fake name...',
                }).set({ Authorization: adminToken });

                expect(response.status).to.be.equal(201);
                expect(Object.keys(response.body.updatedTvProgram)).to.be.deep.equal([
                    'id',
                    'name',
                    'description',
                    'rating',
                    'imageUrl',
                    'ageRating',     
                    'addedOn',
                    'categories',
                ]);
            });
        });

        describe ('[GET] /api/program/:id', () => {
            it ('should respond with 404 status if program does not exists', async () => {
                const response = await chai.request(server).get('/api/program/101').set({ Authorization: adminToken });

                expect(response.status).to.be.equal(404);
                expect(Object.keys(response.body.error)).to.be.deep.equal([
                    'message'
                ]);
            });

            it ('should respond with 200 status and proper body', async () => {
                const response = await chai.request(server).get('/api/program/1').set({ Authorization: adminToken });

                expect(response.status).to.be.equal(200);
                expect(Object.keys(response.body.tvProgram)).to.be.deep.equal([
                    'id',
                    'name',
                    'description',
                    'rating',
                    'imageUrl',
                    'ageRating',     
                    'addedOn',
                    'categories',
                ]);
            });
        });

        it ('[GET] /api/program should respond with 200 status and proper body', async () => {
            const response = await chai.request(server).get('/api/program').set({ Authorization: adminToken });

            expect(response.status).to.be.equal(200);
            expect(response.body.tvPrograms).to.be.instanceOf(Array);
        });

        it ('[GET] /api/program/byCategory/:id should respond with 200 status and proper body', async () => {
            const response = await chai.request(server).get('/api/program/byCategory/1').set({ Authorization: adminToken });

            expect(response.status).to.be.equal(200);
            expect(response.body.tvProgramsByCategory).to.be.instanceOf(Array);
        });

        describe ('[DELETE] /api/category/:id', () => {
            it ('should respond with 404 status if program does not exists', async () => {
                const response = await chai.request(server).delete('/api/program/101').set({ Authorization: adminToken });

                expect(response.status).to.be.equal(404);
                expect(Object.keys(response.body.error)).to.be.deep.equal([
                    'message'
                ]);
            });

            it ('should respond with 201 status and proper body', async () => {
                const response = await chai.request(server).delete('/api/program/1').set({ Authorization: adminToken });

                expect(response.status).to.be.equal(201);
                expect(Object.keys(response.body)).to.be.deep.equal([
                    'removedTvProgramId'
                ]);
            });
        });
    });

    after(async () => {
        const connection = container.take('connection');
        await connection.dropDatabase();
    });
});