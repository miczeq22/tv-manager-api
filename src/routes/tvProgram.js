import * as tvProgramService from '../services/tvProgramService';

const tvProgramRoutes = () => {
    return {
        saveTvProgram: {
            path: '/api/program',
            methods: ['POST'],
            callback: tvProgramService.save,
        },
        updateTvProgram: {
            path: '/api/program/:id',
            methods: ['PUT'],
            callback: tvProgramService.update,
        },
        findOneTvProgram: {
            path: '/api/program/:id',
            methods: ['GET'],
            callback: tvProgramService.findOne,
        },
        findAllTvPrograms: {
            path: '/api/program',
            methods: ['GET'],
            callback: tvProgramService.findAll,
        },
        findAllTvProgramsByCategory: {
            path: '/api/program/byCategory/:id',
            methods: ['GET'],
            callback: tvProgramService.findAllByCategory,
        },
        removeTvProgram: {
            path: '/api/program/:id',
            methods: ['DELETE'],
            callback: tvProgramService.remove,
        },
    };
};

module.exports = tvProgramRoutes;