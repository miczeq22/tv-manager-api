import * as userService from '../services/userService';

const userRoutes = () => {
    return {
        saveUser: {
            path: '/api/user',
            methods: ['POST'],
            callback: userService.save,
        },
        updateUser: {
            path: '/api/user/:id',
            methods: ['PUT'],
            callback: userService.update,
        },
        findOneUser: {
            path: '/api/user/:id',
            methods: ['GET'],
            callback: userService.findOne,
        },
        findAllUsers: {
            path: '/api/user',
            methods: ['GET'],
            callback: userService.findAll,
        },
        removeUser: {
            path: '/api/user/:id',
            methods: ['DELETE'],
            callback: userService.remove,
        },
    };
};

module.exports = userRoutes;