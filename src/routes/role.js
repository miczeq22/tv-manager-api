import * as roleService from '../services/roleService';

const roleRoutes = () => {
    return {
        saveRole: {
            path: '/api/role',
            methods: ['POST'],
            callback: roleService.save,
        },
        updateRole: {
            path: '/api/role/:id',
            methods: ['PUT'],
            callback: roleService.update,
        },
        findOneRole: {
            path: '/api/role/:id',
            methods: ['GET'],
            callback: roleService.findOne,
        },
        findOneRoleByName: {
            path: '/api/role/byName/:name',
            methods: ['GET'],
            callback: roleService.findOneByName,
        },
        findAllRoles: {
            path: '/api/role',
            methods: ['GET'],
            callback: roleService.findAll,
        },
        removeRole: {
            path: '/api/role/:id',
            methods: ['DELETE'],
            callback: roleService.remove,
        }
    };
};

module.exports = roleRoutes;