import validate from '../validation/validate';

const errorResponse = (status, message) => {
    return {
        status,
        body: {
            error: {
                message,
            },
        },
    };
};

export const save = async (req, { tvstationRepository }) => {
    try {
        const data = await validate('insertTvStation', req.body);
        const tvStation = await tvstationRepository.findOne({ where: { name: data.name } });

        if (tvStation) {
            return errorResponse(400, `Tv Station: "${data.name} already exists."`);
        }

        const savedTvStation = await tvstationRepository.save(data);

        return {
            status: 201,
            body: {
                savedTvStation,
            },
        };
    } catch (error) {
        return errorResponse(400, error.message);
    }
};

export const update = async (req, { tvstationRepository }) => {
    try {
        const data = await validate('insertTvStation', req.body);
        const tvStation = await tvstationRepository.findOne({ where: { id: req.params.id } });

        if (!tvStation) {
            return errorResponse(404, `Tv Station with id: "${req.params.id}" does not exists.`);
        }

        tvStation.name = data.name;

        const updatedTvStation = await tvstationRepository.save(tvStation);

        return {
            status: 201,
            body: {
                updatedTvStation,
            },
        };
    } catch (error) {
        return errorResponse(400, error.message);
    }
};

export const findOne = async (req, { tvstationRepository }) => {
    try {
        const tvStation = await tvstationRepository.findOne({ where: { id: req.params.id } });

        if (!tvStation) {
            return errorResponse(404, `Tv Station with id: "${req.params.id}" does not exists.`);
        }

        return {
            status: 200,
            body: {
                tvStation,
            },
        };
    } catch (error) {
        return errorResponse(500, error.message);
    }
};

export const findAll = async (req, { tvstationRepository }) => {
    try {
        const tvStations = await tvstationRepository.find();

        return {
            status: 200,
            body: {
                tvStations,
            },
        };
    } catch (error) {
        return errorResponse(500, error.message);
    }
};

export const remove = async (req, { tvstationRepository }) => {
    try {
        const tvStation = await tvstationRepository.findOne({ where: { id: req.params.id } });

        if (!tvStation) {
            return errorResponse(404, `Tv Station with id: "${req.params.id}" does not exists.`);
        }

        await tvstationRepository.removeById(tvStation.id);

        return {
            status: 201,
            body: {
                removedTvStationId: tvStation.id,
            },
        };
    } catch (error) {
        return errorResponse(500, error.message);
    }
};
