import validate from '../validation/validate';

const errorResponse = (status, message) => {
    return {
        status,
        body: {
            error: {
                message,
            },
        },
    };
};


export const save = async (req, { categoryRepository }) => {
    try {
        const data = await validate('insertCategory', req.body);
        const category = await categoryRepository.findOne({ where: { name: data.name } });

        if (category) {
            return errorResponse(400, `Category: "${data.name}" already exists.`);
        }

        const savedCategory = await categoryRepository.save(data);

        return {
            status: 201,
            body: {
                savedCategory,
            },
        };
    } catch (error) {
        return errorResponse(400, error.message);
    }
};

export const update = async (req, { categoryRepository }) => {
    try {
        const data = await validate('insertCategory', req.body);
        const category = await categoryRepository.findOne({ where: { id: req.params.id } });

        if (!category) {
            return errorResponse(404, `Category with id: "${req.params.id}" does not exists`);
        }

        category.name = data.name;

        const updatedCategory = await categoryRepository.save(category);

        return {
            status: 201,
            body: {
                updatedCategory,
            },
        };
    } catch (error) {
        return errorResponse(400, error.message);
    }
};

export const findOne = async (req, { categoryRepository }) => {
    try {
        const category = await categoryRepository.findOne({ where: { id: req.params.id } });

        if (!category) {
            return errorResponse(404, `Category with id: "${req.params.id}" does not exists`);
        }

        return {
            status: 200,
            body: {
                category,
            },
        };
    } catch (error) {
        return errorResponse(500, error.message);
    }
};

export const findAll = async (req, { categoryRepository }) => {
    try {
        const categories = await categoryRepository.find();

        return {
            status: 200,
            body: {
                categories,
            },
        };
    } catch (error) {
        return errorResponse(500, error.message);
    }
};

export const remove = async (req, { categoryRepository }) => {
    try {
        const category = await categoryRepository.findOne({ where: { id: req.params.id } });

        if (!category) {
            return errorResponse(404, `Category with id: "${req.params.id}" does not exists`);
        }

        await categoryRepository.removeById(category.id);

        return {
            status: 201,
            body: {
                removedCategoryId: category.id,
            },
        };

    } catch (error) {
        return errorResponse(500, error.message);
    }
};
