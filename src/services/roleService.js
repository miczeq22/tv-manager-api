import validate from '../validation/validate';

const errorResponse = (status, message) => {
    return {
        status,
        body: {
            error: {
                message,
            },
        },
    };
};

export const save = async (req, { roleRepository }) => {
    try {
        const data = await validate('insertRole', req.body);
        const role = await roleRepository.findOne({ where: { name: data.name } });

        if (role) {
            return errorResponse(400, `Role: "${data.name}" already exists.`);
        }

        const savedRole = await roleRepository.save(data);

        return {
            status: 201,
            body: {
                savedRole,
            },
        };
    } catch (error) {
        return errorResponse(400, error.message);
    }
};

export const update = async (req, { roleRepository }) => {
    try {
        const data = await validate('insertRole', req.body);
        const role = await roleRepository.findOne({ where: { id: req.params.id } });

        if (!role) {
            return errorResponse(404, `Role with id: "${req.params.id}" does not exists.`);
        }

        role.name = data.name;

        const updatedRole = await roleRepository.save(role);

        return {
            status: 201,
            body: {
                updatedRole,
            },
        };
    } catch (error) {
        return errorResponse(400, error.message);
    }
};

export const findOne = async (req, { roleRepository }) => {
    try {
        const role = await roleRepository.findOne({ where: { id: req.params.id } });

        if (!role) {
            return errorResponse(404, `Role with id: "${req.params.id}" does not exists.`);
        }

        return {
            status: 200,
            body: {
                role,
            },
        };
    } catch (error) {
        return errorResponse(500, error.message);
    }
};

export const findOneByName = async (req, { roleRepository }) => {
    try {
        const role = await roleRepository.findOne({ where: { name: req.params.name } });

        if (!role) {
            return errorResponse(404, `Role: "${req.params.name}" does not exists.`);
        }

        return {
            status: 200,
            body: {
                role,
            },
        };
    } catch (error) {
        return errorResponse(500, error.message);
    }
};

export const findAll = async (req, { roleRepository }) => {
    try {
        const roles = await roleRepository.find();

        return {
            status: 200,
            body: {
                roles,
            },
        };
    } catch (error) {
        return errorResponse(500, error.message);
    }
};

export const remove = async (req, { roleRepository }) => {
    try {
        const role = await roleRepository.findOne({ where: { id: req.params.id } });

        if (!role) {
            return errorResponse(404, `Role with id: "${req.params.id}" does not exists.`);
        }

        await roleRepository.removeById(role.id);

        return {
            status: 201,
            body: {
                removedRoleId: role.id,
            },
        };
    } catch (error) {
        return errorResponse(500, error.message);
    }
};
