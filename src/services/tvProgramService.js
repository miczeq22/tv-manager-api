import validate from '../validation/validate';

const errorResponse = (status, message) => {
    return {
        status,
        body: {
            error: {
                message,
            },
        },
    };
};

export const save = async (req, { tvprogramRepository }) => {
    try {
        await validate('insertTvProgram', req.body);
        const existingTvProgram = await tvprogramRepository.findOne({ where: { name: req.body.name } });

        if (existingTvProgram) {
            return errorResponse(400, `Tv Program: "${req.body.name}" already exists.`);
        }

        const savedTvProgram = await tvprogramRepository.save(req.body);

        return {
            status: 201,
            body: {
                savedTvProgram,
            },
        };
    } catch (error) {
        return errorResponse(400, error.message);
    }
};

export const update = async (req, { tvprogramRepository }) => {
    try {
        const tvProgram = await tvprogramRepository.findOne({ where: { id: req.params.id }, relations: ['categories', 'tvStation'] });

        if (!tvProgram) {
            return errorResponse(404, `Tv Program with id: "${req.params.id}" does not exists.`);
        }

        const data = req.body;

        if (data.name) tvProgram.name = data.name;
        if (data.description) tvProgram.description = data.description;
        if (data.rating) tvProgram.rating = data.rating;
        if (data.imageUrl) tvProgram.imageUrl = data.imageUrl;
        if (data.ageRating) tvProgram.ageRating = data.ageRating;
        if (data.categories) tvProgram.categories = data.categories;
        if (data.tvStation) tvProgram.tvStation = data.tvStation;

        const updatedTvProgram = await tvprogramRepository.save(tvProgram);

        return {
            status: 201,
            body: {
                updatedTvProgram,
            },
        };
    } catch (error) {
        return errorResponse(400, error.message);
    }
};

export const findOne = async (req, { tvprogramRepository }) => {
    try {
        const tvProgram = await tvprogramRepository.findOne({ where: { id: req.params.id }, relations: ['categories', 'tvStation'] });

        if (!tvProgram) {
            return errorResponse(404, `Tv program with id: "${req.params.id}" does not exists.`);
        }

        return {
            status: 200,
            body: {
                tvProgram,
            },
        };
    } catch (error) {
        return errorResponse(500, error.message);
    }
};

export const findAll = async (req, { tvprogramRepository }) => {
    try {
        const tvPrograms = await tvprogramRepository.find({ relations: ['categories', 'tvStation'] });

        return {
            status: 200,
            body: {
                tvPrograms,
            },
        };
    } catch (error) {
        return errorResponse(500, error.message);
    }
};

export const findAllByCategory = async (req, { tvprogramRepository }) => {
    try {
        const tvPrograms = await tvprogramRepository.find({ relations: ['categories', 'tvStation'] });

        const tvProgramsByCategory = []; 
        tvPrograms.forEach(program => {
            return program.categories.forEach(category => {
                if (category.id === parseInt(req.params.id, 10)) {
                    tvProgramsByCategory.push(program);
                }
            })
        });

        return {
            status: 200,
            body: {
                tvProgramsByCategory,
            },
        };
    } catch (error) {
        return errorResponse(500, error.message);
    }
};

export const remove = async (req, { tvprogramRepository, connection }) => {
    try {
        const tvProgram = await tvprogramRepository.findOne({ where: { id: req.params.id } });

        if (!tvProgram) {
            return errorResponse(404, `Tv program with id: "${req.params.id}" does not exists.`);
        }

        await connection.createQueryBuilder().delete().from('program_category', 'program_category')
        .where('program_category.tvProgramId=:id', { id: tvProgram.id }).execute();

        await tvprogramRepository.removeById(tvProgram.id);

        return {
            status: 201,
            body: {
                removedTvProgramId: tvProgram.id,
            },
        };
    } catch (error) {
        return errorResponse(500, error.message);
    }
};
