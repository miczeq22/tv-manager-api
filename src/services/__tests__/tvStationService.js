import { assert, expect } from 'chai';
import sinon from 'sinon';
import * as tvStationService from '../tvStationService';

describe ('TvStation Service', () => {
    let fakeRepository = {};

    beforeEach(() => {
        fakeRepository = {
            find: args => fakeRepository,
            findOne: args => fakeRepository,
        };
    });

    describe ('save method', () => {
        const req = {
            body: {
                name: 'Polsat',
            },
        };

        it ('should execute findOne({ where: { name } }) on repository object', async () => {
            const findOne = sinon.stub();
            findOne.resolves(true);
            fakeRepository.findOne = findOne;

            await tvStationService.save(req, { tvstationRepository: fakeRepository });
            assert(findOne.calledWith({ where: { name: req.body.name } }));
        });

        it ('should execute save(tvStation) on repository object', async () => {
            const findOne = sinon.stub();
            findOne.resolves(false);
            fakeRepository.findOne = findOne;
            const save = sinon.stub();
            save.resolves(true);
            fakeRepository.save = save;

            await tvStationService.save(req, { tvstationRepository: fakeRepository });
            assert(save.calledWith(req.body));
        });
    });

    describe ('update method', () => {
        const req = {
            params: {
                id: 1,
            },
            body: {
                name: 'polsat',
            },
        };

        it ('should execute findOne({ where: { id } }) on repository object', async () => {
            const findOne = sinon.stub();
            findOne.resolves(true);
            fakeRepository.findOne = findOne;

            await tvStationService.update(req, { tvstationRepository: fakeRepository });
            assert(findOne.calledWith({ where: { id: req.params.id } }));
        });

        it ('should execute save(tvStation) on repository object', async () => {
            const findOne = sinon.stub();
            findOne.resolves({ name: 'Polsat' });
            fakeRepository.findOne = findOne;

            const save = sinon.stub();
            save.resolves(true);
            fakeRepository.save = save;

            await tvStationService.update(req, { tvstationRepository: fakeRepository });
            assert(save.calledWith(req.body));
        });
    });

    it ('findOne should execute findOne({ where: { id } }) on repository object', async () => {
        const findOne = sinon.stub();
        findOne.resolves(true);
        fakeRepository.findOne = findOne;

        const req = {
            params: {
                id: 1,
            },
        };

        await tvStationService.findOne(req, { tvstationRepository: fakeRepository });
        assert(findOne.calledWith({ where: { id: req.params.id } }));
    });

    it ('findAll should execute findOne() on repository object', async () => {
        const find = sinon.stub();
        find.resolves(true);
        fakeRepository.find = find;

        const req = {};

        await tvStationService.findAll(req, { tvstationRepository: fakeRepository });
        expect(find.calledOnce).to.be.equal(true);
    });

    describe ('remove method', () => {
        const req = {
            params: {
                id: 1,
            },
        };

        it ('should execute findOne({ where: { id } }) on repository object', async () => {
            const findOne = sinon.stub();
            findOne.resolves(true);
            fakeRepository.findOne = findOne;

            await tvStationService.remove(req, { tvstationRepository: fakeRepository });
            assert(findOne.calledWith({ where: { id: req.params.id } }));
        });

        it ('should execute removeById(id) on repository object', async () => {
            const findOne = sinon.stub();
            findOne.resolves({ id: req.params.id });
            fakeRepository.findOne = findOne;

            const removeById = sinon.stub();
            removeById.resolves(true);
            fakeRepository.removeById = removeById;

            await tvStationService.remove(req, { tvstationRepository: fakeRepository });
            assert(removeById.calledWith(req.params.id));
        });
    });
});