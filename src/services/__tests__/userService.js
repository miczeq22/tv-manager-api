import { assert, expect } from 'chai';
import sinon from 'sinon';
import * as userService from '../userService';

describe ('User Service', () => {
    let fakeRepository = {};

    beforeEach(() => {
        fakeRepository = {
            find: args => fakeRepository,
            findOne: args => fakeRepository,
        };
    });

    describe ('save method', () => {
        const req = {
            body: {
                username: 'admin',
                password: 'admin123',
            },
        };

        it ('should execute findOne({ where: { username } }) on repository object', async () => {
            const findOne = sinon.stub();
            findOne.resolves({ roles: [] });
            fakeRepository.findOne = findOne;

            await userService.save(req, { userRepository: fakeRepository });
            assert(findOne.calledWith({ where: { username: req.body.username } }));
        });

        it ('should execute save(role) on repository object', async () => {
            const findOne = sinon.stub();
            findOne.resolves(false);
            fakeRepository.findOne = findOne;
            const save = sinon.stub();
            save.resolves(true);
            fakeRepository.save = save;

            const findOneRole = sinon.stub();
            findOneRole.resolves({});

            await userService.save(req, { userRepository: fakeRepository, roleRepository: { findOne: findOneRole } });
            expect(save.calledOnce).to.be.equal(true);
        });
    });

    describe ('update method', () => {
        const req = {
            params: {
                id: 1,
            },
            body: {
                oldPassword: 'admin123',
                newPassword: 'admin1234'
            },
        };

        it ('should execute findOne({ where: { id } }) on repository object', async () => {
            const findOne = sinon.stub();
            findOne.resolves(true);
            fakeRepository.findOne = findOne;

            await userService.update(req, { userRepository: fakeRepository });
            assert(findOne.calledWith({ where: { id: req.params.id } }));
        });
    });

    it ('findOne should execute findOne({ where: { id } }) on repository object', async () => {
        const findOne = sinon.stub();
        findOne.resolves(true);
        fakeRepository.findOne = findOne;

        const req = {
            params: {
                id: 1,
            },
        };

        await userService.findOne(req, { userRepository: fakeRepository });
        assert(findOne.calledWith({ where: { id: req.params.id } }));
    });

    it ('findAll should execute find() on repository object', async () => {
        const find = sinon.stub();
        find.resolves(true);
        fakeRepository.find = find;

        const req = {};

        await userService.findAll(req, { userRepository: fakeRepository });
        expect(find.calledOnce).to.be.equal(true);
    });

    describe ('remove method', () => {
        const req = {
            params: {
                id: 1,
            },
        };

        it ('should execute findOne({ where: { id } }) on repository object', async () => {
            const findOne = sinon.stub();
            findOne.resolves(true);
            fakeRepository.findOne = findOne;

            await userService.remove(req, { userRepository: fakeRepository });
            assert(findOne.calledWith({ where: { id: req.params.id } }));
        });

        it ('should execute removeById(id) on repository object', async () => {
            const findOne = sinon.stub();
            findOne.resolves({ id: req.params.id });
            fakeRepository.findOne = findOne;

            const removeById = sinon.stub();
            removeById.resolves(true);
            fakeRepository.removeById = removeById;

            await userService.remove(req, { userRepository: fakeRepository });
            assert(removeById.calledWith(req.params.id));
        });
    });
});