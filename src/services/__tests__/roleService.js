import { assert, expect } from 'chai';
import sinon from 'sinon';
import * as roleService from '../roleService';

describe ('Role Service', () => {
    let fakeRepository = {};

    beforeEach(() => {
        fakeRepository = {
            find: args => fakeRepository,
            findOne: args => fakeRepository,
        };
    });

    describe ('save method', () => {
        const req = {
            body: {
                name: 'Admin',
            },
        };

        it ('should execute findOne({ where: { name } }) on repository object', async () => {
            const findOne = sinon.stub();
            findOne.resolves(true);
            fakeRepository.findOne = findOne;

            await roleService.save(req, { roleRepository: fakeRepository });
            assert(findOne.calledWith({ where: { name: req.body.name } }));
        });

        it ('should execute save(role) on repository object', async () => {
            const findOne = sinon.stub();
            findOne.resolves(false);
            fakeRepository.findOne = findOne;
            const save = sinon.stub();
            save.resolves(true);
            fakeRepository.save = save;

            await roleService.save(req, { roleRepository: fakeRepository });
            assert(save.calledWith(req.body));
        });
    });

    describe ('update method', () => {
        const req = {
            params: {
                id: 1,
            },
            body: {
                name: 'admin',
            },
        };

        it ('should execute findOne({ where: { id } }) on repository object', async () => {
            const findOne = sinon.stub();
            findOne.resolves(true);
            fakeRepository.findOne = findOne;

            await roleService.update(req, { roleRepository: fakeRepository });
            assert(findOne.calledWith({ where: { id: req.params.id } }));
        });

        it ('should execute save(role) on repository object', async () => {
            const findOne = sinon.stub();
            findOne.resolves({ name: 'Admin' });
            fakeRepository.findOne = findOne;

            const save = sinon.stub();
            save.resolves(true);
            fakeRepository.save = save;

            await roleService.update(req, { roleRepository: fakeRepository });
            assert(save.calledWith(req.body));
        });
    });

    it ('findOne should execute findOne({ where: { id } }) on repository object', async () => {
        const findOne = sinon.stub();
        findOne.resolves(true);
        fakeRepository.findOne = findOne;

        const req = {
            params: {
                id: 1,
            },
        };

        await roleService.findOne(req, { roleRepository: fakeRepository });
        assert(findOne.calledWith({ where: { id: req.params.id } }));
    });

    it ('findOneByName should execute findOne({ where: { name } }) on repository object', async () => {
        const findOne = sinon.stub();
        findOne.resolves(true);
        fakeRepository.findOne = findOne;

        const req = {
            params: {
                name: 'admin',
            },
        };

        await roleService.findOneByName(req, { roleRepository: fakeRepository });
        assert(findOne.calledWith({ where: { name: req.params.name } }));
    });

    it ('findAll should execute findOne() on repository object', async () => {
        const find = sinon.stub();
        find.resolves(true);
        fakeRepository.find = find;

        const req = {};

        await roleService.findAll(req, { roleRepository: fakeRepository });
        expect(find.calledOnce).to.be.equal(true);
    });

    describe ('remove method', () => {
        const req = {
            params: {
                id: 1,
            },
        };

        it ('should execute findOne({ where: { id } }) on repository object', async () => {
            const findOne = sinon.stub();
            findOne.resolves(true);
            fakeRepository.findOne = findOne;

            await roleService.remove(req, { roleRepository: fakeRepository });
            assert(findOne.calledWith({ where: { id: req.params.id } }));
        });

        it ('should execute removeById(id) on repository object', async () => {
            const findOne = sinon.stub();
            findOne.resolves({ id: req.params.id });
            fakeRepository.findOne = findOne;

            const removeById = sinon.stub();
            removeById.resolves(true);
            fakeRepository.removeById = removeById;

            await roleService.remove(req, { roleRepository: fakeRepository });
            assert(removeById.calledWith(req.params.id));
        });
    });
});