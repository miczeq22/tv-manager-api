import { assert, expect } from 'chai';
import sinon from 'sinon';
import * as tvProgramService from '../tvProgramService';

describe ('TvProgram Service', () => {
    let fakeRepository = {};

    beforeEach(() => {
        fakeRepository = {
            find: args => fakeRepository,
            findOne: args => fakeRepository,
        };
    });

    describe ('save method', () => {
        const req = {
            body: {
                name: 'Fake Tv Program',
                description: 'Lorem ipsum...',
                rating: 7,
                imageUrl: 'Fake Url...',
                ageRating: 13,
            },
        };

        it ('should execute findOne({ where: { name } }) on repository object', async () => {
            const findOne = sinon.stub();
            findOne.resolves(true);
            fakeRepository.findOne = findOne;

            await tvProgramService.save(req, { tvprogramRepository: fakeRepository });
            assert(findOne.calledWith({ where: { name: req.body.name } }));
        });

        it ('should execute save(role) on repository object', async () => {
            const findOne = sinon.stub();
            findOne.resolves(false);
            fakeRepository.findOne = findOne;
            const save = sinon.stub();
            save.resolves(true);
            fakeRepository.save = save;

            await tvProgramService.save(req, { tvprogramRepository: fakeRepository });
            assert(save.calledWith(req.body));
        });
    });

    describe ('update method', () => {
        const req = {
            params: {
                id: 1,
            },
            body: {
                name: 'Fake Tv Program',
                description: 'Lorem ipsum...',
                rating: 7,
                imageUrl: 'Fake Url...',
                ageRating: 13,
            },
        };

        it ('should execute findOne({ where: { id } }) on repository object', async () => {
            const findOne = sinon.stub();
            findOne.resolves({});
            fakeRepository.findOne = findOne;

            const save = () => true;
            fakeRepository.save = save;

            await tvProgramService.update(req, { tvprogramRepository: fakeRepository });

            assert(findOne.calledWith({ where: { id: req.params.id }, relations: ['categories', 'tvStation'] }));
        });

        it ('should execute save(role) on repository object', async () => {
            const findOne = sinon.stub();
            findOne.resolves({ name: 'Admin' });
            fakeRepository.findOne = findOne;

            const save = sinon.stub();
            save.resolves(true);
            fakeRepository.save = save;

            await tvProgramService.update(req, { tvprogramRepository: fakeRepository });
            expect(save.calledOnce).to.be.equal(true);
        });
    });

    it ('findOne should execute findOne({ where: { id } }) on repository object', async () => {
        const findOne = sinon.stub();
        findOne.resolves(true);
        fakeRepository.findOne = findOne;

        const req = {
            params: {
                id: 1,
            },
        };

        await tvProgramService.findOne(req, { tvprogramRepository: fakeRepository });
        assert(findOne.calledWith({ where: { id: req.params.id }, relations: ['categories', 'tvStation'] }));
    });

    it ('findAll should execute findOne() on repository object', async () => {
        const find = sinon.stub();
        find.resolves(true);
        fakeRepository.find = find;

        const req = {};

        await tvProgramService.findAll(req, { tvprogramRepository: fakeRepository });
        expect(find.calledOnce).to.be.equal(true);
    });

    it ('findAllByCategory should execute findOne() on repository object', async () => {
        const find = sinon.stub();
        find.resolves(true);
        fakeRepository.find = find;

        const req = {};

        await tvProgramService.findAll(req, { tvprogramRepository: fakeRepository });
        assert(find.calledWith({ relations: ['categories', 'tvStation'] }));
    });

    describe ('remove method', () => {
        const req = {
            params: {
                id: 1,
            },
        };

        const connection = {
            createQueryBuilder: () => connection,
            delete: () => connection,
            from: () => connection,
            where: () => connection,
            execute: () => connection,
        };

        it ('should execute findOne({ where: { id } }) on repository object', async () => {
            const findOne = sinon.stub();
            findOne.resolves(true);
            fakeRepository.findOne = findOne;

            const removeById = sinon.stub();
            removeById.resolves(true);
            fakeRepository.removeById = removeById;

            console.log(await tvProgramService.remove(req, { tvprogramRepository: fakeRepository, connection }));
            assert(findOne.calledWith({ where: { id: req.params.id } }));
        });

        it ('should execute removeById(id) on repository object', async () => {
            const findOne = sinon.stub();
            findOne.resolves({ id: req.params.id });
            fakeRepository.findOne = findOne;

            const removeById = sinon.stub();
            removeById.resolves(true);
            fakeRepository.removeById = removeById;

            await tvProgramService.remove(req, { tvprogramRepository: fakeRepository, connection });
            assert(removeById.calledWith(req.params.id));
        });
    });
});