import validate from '../validation/validate';
import bcrypt from 'bcrypt';

const errorResponse = (status, message) => {
    return {
        status,
        body: {
            error: {
                message,
            },
        },
    };
};

export const save = async (req, { userRepository, roleRepository }) => {
    try {
        const data = await validate('insertUser', req.body);
        const user = await userRepository.findOne({ where: { username: data.username } });

        if (user) {
            return errorResponse(400, `User: "${data.username}" already exists.`);
        }

        const passwordHash = await bcrypt.hash(data.password, 10);
        const userRole = await roleRepository.findOne({ where: { name: 'User' } });

        const savedUser = await userRepository.save({ username: data.username, passwordHash, roles: userRole ? [userRole] : [] });

        return {
            status: 201,
            body: {
                savedUser,
            },
        };
    } catch (error) {
        return errorResponse(400, error.message);
    }
};

export const update = async (req, { userRepository }) => {
    try {
        const data = await validate('updateUser', req.body);
        const user = await userRepository.findOne({ where: { id: req.params.id } });
        
        if (!user) {
            return errorResponse(404, `User with id: "${req.params.id}" does not exists.`);
        }

        const validPassword = await bcrypt.compare(data.oldPassword, user.passwordHash);

        if (!validPassword) {
            return errorResponse(403, 'Unauthorized');
        }

        const passwordHash = await bcrypt.hash(data.newPassword, 10);

        user.passwordHash = passwordHash;

        const updatedUser = await userRepository.save(user);

        return {
            status: 201,
            body: {
                updatedUser,
            },
        };
    } catch (error) {
        return errorResponse(400, error.message);
    }
};

export const findOne = async (req, { userRepository }) => {
    try {
        const user = await userRepository.findOne({ where: { id: req.params.id } });

        if (!user) {
            return errorResponse(404, `User with id: "${req.params.id}" does not exists.`);
        }

        return {
            status: 200,
            body: {
                user,
            },
        };
    } catch (error) {
        return errorResponse(500, error.message);
    }
};

export const findAll = async (req, { userRepository }) => {
    try {
        const users = await userRepository.find();

        return {
            status: 200,
            body: {
                users,
            },
        };
    } catch (error) {
        return errorResponse(500, error.message);
    }
};

export const remove = async (req, { userRepository }) => {
    try {
        const user = await userRepository.findOne({ where: { id: req.params.id } });

        if (!user) {
            return errorResponse(404, `User with id: "${req.params.id}" does not exists.`);
        }

        await userRepository.removeById(user.id);

        return {
            status: 201,
            body: {
                removedUserId: user.id,
            },
        };
    } catch (error) {
        return errorResponse(500, error.message);
    }
};
