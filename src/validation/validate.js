import validationFactory from '@brainhubeu/hadron-validation';

import insertRole from './schemas/insertRole.json';
import insertUser from './schemas/insertUser.json';
import updateUser from './schemas/updateUser.json';
import insertTvStation from './schemas/insertTvStation.json';
import insertCategory from './schemas/insertCategory';
import insertTvProgram from './schemas/insertTvProgram.json';

export default validationFactory({
    insertRole,
    insertUser,
    updateUser,
    insertTvStation,
    insertCategory,
    insertTvProgram,
});
