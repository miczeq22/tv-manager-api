import hadron from '@brainhubeu/hadron-core';
import express from 'express';
import * as hadronExpress from '@brainhubeu/hadron-express';
import * as hadronTypeOrm from '@brainhubeu/hadron-typeorm';
import db from './config/db';
import testDb from './config/test_db';
import jsonProvider from '@brainhubeu/hadron-json-provider';
import cors from 'cors';
import bodyParser from 'body-parser';
import http from 'http';
import * as hadronAuth from '@brainhubeu/hadron-security';
import loginMiddleware from './auth/loginMiddleware';
import bcrypt from 'bcrypt';

const port = process.env.PORT || 2000;

const expressApp = express();
expressApp.use(cors());
expressApp.use(bodyParser.json());

const database = process.env.NODE_ENV === 'DEV' ? db : testDb;
const server = http.createServer(expressApp);

let container = {};

const hadronInit = async () => {
    const routes = await jsonProvider([`${__dirname}/routes/*`], ['js']);

    const config = {
        ...database,
        routes: {
            loginRoute: {
                path: '/login',
                methods: ['POST'],
                callback: loginMiddleware,
            },
            ...routes,
        },
        securedRoutes: [
            {
                path: '/api/role/**',
                roles: 'Admin'
            },
            {
                path: '/api/category/**',
                roles: 'Admin',
            },
            {
                path: '/api/station/**',
                roles: 'Admin',
            },
            {
                path: '/api/program/**',
                roles: 'Admin',
            },
        ],
    };
    

    container = await hadron(expressApp, [hadronAuth, hadronExpress, hadronTypeOrm], config);

    expressApp.use((req, res, next) => {
        return res.status(404).json({
            error: {
                message: 'Not Found.',
            },
        });
    });

    server.listen(port, () => {
        expressApp.emit('appStarted');
        console.log(`Listening on http://localhost:${port}`);
    });
}

hadronInit();

export default expressApp;
export { container };