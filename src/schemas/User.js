const userSchema = {
    name: 'User',
    columns: {
        id: {
            primary: true,
            type: 'int',
            generated: true,
        },
        username: {
            type: 'varchar',
            unique: true,
        },
        passwordHash: {
            type: 'varchar',
        },
        addedOn: {
            type: 'timestamp'
        },
    },
    relations: {
        roles: {
            target: 'Role',
            type: 'many-to-many',
            joinTable: {
                name: 'user_role',
            },
            onDelete: 'CASCADE',
        }
    }
};

export default userSchema;
