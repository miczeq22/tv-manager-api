const tvProgramSchema = {
    name: 'TvProgram',
    columns: {
        id: {
            primary: true,
            type: 'int',
            generated: true,
        },
        name: {
            type: 'varchar',
            unique: true,
            required: true,
        },
        description: {
            type: 'varchar',
            required: true,
        },
        rating: {
            type: 'int',
            required: true,
        },
        imageUrl: {
            type: 'varchar',
            required: true,
        },
        ageRating: {
            type: 'int',
            required: true,
        },
        addedOn: {
            type: 'timestamp',
        },
    },
    relations: {
        categories: {
            target: 'Category',
            type: 'many-to-many',
            joinTable: {
                name: 'program_category',
            },
        },
        tvStation: {
            target: 'TvStation',
            type: 'one-to-one',
            joinColumn: true,
        },
    }
};

export default tvProgramSchema;
