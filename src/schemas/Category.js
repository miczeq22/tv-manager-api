const categorySchema = {
    name: 'Category',
    columns: {
        id: {
            primary: true,
            type: 'int',
            generated: true,
        },
        name: {
            type: 'varchar',
            unique: true,
        },
        addedOn: {
            type: 'timestamp',
        },
    },
};

export default categorySchema;
