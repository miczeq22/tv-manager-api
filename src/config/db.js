import userSchema from '../schemas/User';
import roleSchema from '../schemas/Role';
import tvStationSchema from '../schemas/TvStation';
import categorySchema from '../schemas/Category';
import tvProgramSchema from '../schemas/TvProgram';

const connection = {
  name: 'mysql-connection',
  type: 'mysql',
  host: 'localhost',
  port: 3306,
  username: 'root',
  password: 'my-secret-pw',
  database: 'tv-manager',
  entitySchemas: [userSchema, roleSchema, tvStationSchema, categorySchema, tvProgramSchema],
  synchronize: true,
};

export default {
    connection,
};
